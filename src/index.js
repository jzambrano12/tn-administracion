import "./polyfill";
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

const registerAndRunApp = () => {
  const appContainer = document.getElementById("root");

  if (appContainer === null) {
    const message = "Container '#app' not found";
    console.error(message);
    throw Error(message);
  }
  ReactDOM.render(<App />, appContainer);
};

if (process.env.NODE_ENV !== "production" && module.hot) {
  module.hot.accept("./App", registerAndRunApp);
}

registerAndRunApp();
