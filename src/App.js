import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
// Styles
// CoreUI Icons Set
import "@coreui/icons/css/coreui-icons.min.css";
// Import Flag Icons Set
import "flag-icon-css/css/flag-icon.min.css";
// Import Font Awesome Icons Set
import "font-awesome/css/font-awesome.min.css";
// Import Simple Line Icons Set
import "simple-line-icons/css/simple-line-icons.css";
// Import Main styles for this application
import "./scss/style.css";

import { Provider } from "react-redux";
import store, { persistor } from "./configureStore";
// Containers
import { DefaultLayout } from "./containers";
// Pages
import { Login } from "./views/Pages";
import { PersistGate } from "redux-persist/integration/react";

// Checker
import AuthChecker from "./authChecker";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Router>
            <AuthChecker>
              <Switch>
                <Route
                  exact
                  path="/login"
                  name="Login Page"
                  component={Login}
                />
                {/*<Route exact path="/olvido-contraseña" name="¿Olvido su contraseña?" component={Forgot} />*/}
                {/*<Route exact path="/404" name="Page 404" component={Page404} />*/}
                {/*<Route exact path="/500" name="Page 500" component={Page500} />*/}
                <Route path="/" name="Home" component={DefaultLayout} />
              </Switch>
            </AuthChecker>
          </Router>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
