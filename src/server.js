const path = require("path");
const express = require("express");
const port = 3001;
const app = express();

app.use(express.static(path.join(__dirname, "..", "build")));

app.get("*", (req, res) =>
  res.sendFile(path.join(__dirname, "..", "build", "index.html"))
);

// eslint-disable-next-line
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
