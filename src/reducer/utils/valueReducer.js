export default (name, initialState) => {
  const SET = `${name}_SET`;
  const RESET = `${name}_RESET`;

  const set = value => ({ type: SET, payload: value });
  const reset = () => ({ type: RESET });

  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case SET: {
        return action.payload;
      }

      case RESET: {
        return initialState;
      }
      default: {
        return state;
      }
    }
  }

  return {
    actions: {SET, RESET},
    actionCreators: {set, reset},
    reducer
  }
}
