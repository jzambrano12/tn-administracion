import {combineReducers} from 'redux'
import {default as user} from './user'

export default combineReducers({ user })
