import valueReducer from "./utils/valueReducer";

export const { reducer, actions, actionCreators} = valueReducer("USER", {});
export default reducer;
