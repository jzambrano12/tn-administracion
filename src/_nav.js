export default {
  items: [
    {
      name: 'Inicio',
      url: '/panel/editar',
      icon: 'icon-home',
    },
    {
      title: true,
      name: 'ADMINISTRACIÓN',
    },
    {
      name: 'Desactivados',
      icon: '',
      children: [
        {
          name: 'Articulos Desactivados',
          url: '/panel/articulos-desactivados',
          icon: 'icon-trash',
        },
        {
          name: 'Noticias Desactivadas',
          url: '/panel/noticias-desactivadas',
          icon: 'icon-trash',
        },
      ],
    },
    {
      name: 'Eventos',
      icon: '',
      children: [
        {
          name: 'Ver eventos',
          url: '/panel/ver-eventos',
          icon: 'icon-eye',
        },
        {
          name: 'Crear eventos',
          url: '/panel/crear-eventos',
          icon: 'icon-puzzle',
        },
      ],
    }
  ],
};
