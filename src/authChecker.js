import React, { Component } from "react";
import { connect } from "react-redux";
import qs from "query-string";
import { withRouter } from "react-router-dom";
import api from "./api";
import { actionCreators as userActionCreators } from "./reducer/user";
import ContentLoader from "react-content-loader";

class AuthChecker extends Component {
  constructor(props) {
    super(props);
    const { location, user, history, setUser } = props;
    const parsed = qs.parse(location.search);
    let isLoading = false;

    if (parsed.token) {
      api
        .getUserInfo(parsed.token)
        .then(res => {
          const { data, error } = res;
          if (!error) {
            setUser(data.data);
          }
        })
        .then(() => {
          this.setState({
            isLoading: false
          });
          console.log("Api called");
        });
      console.log("Calling Api");
      isLoading = true;
      history.replace(location.pathname);
      // delay(3000).then(() => {
      //   this.setState({
      //     isLoading: false,
      //   })
      //   console.log('Api called')
      // })
    } else if (user.id) {
      console.log("Inicio de sesión correcto");
      //user is logged
    } else {
      // go loggin
      history.push("/login");
    }

    this.state = {
      isLoading
    };
  }

  componentDidUpdate(prevProps) {
    const { user, history } = this.props;
    if (prevProps.user !== user && !user.token) {
      history.push("/login");
    }
  }

  render() {
    const AuthLoading = props => (
      <ContentLoader
        height={969}
        width={1917}
        speed={2}
        primaryColor="#eaebec"
        secondaryColor="#dad8d8"
        {...props}
      >
        <rect
          x="-83.38"
          y="0.67"
          rx="0"
          ry="0"
          width="2003.729"
          height="55.120000000000005"
        />
        <rect x="2.04" y="57.67" rx="0" ry="0" width="263.33" height="932.36" />
        <rect
          x="265.96"
          y="57.67"
          rx="0"
          ry="0"
          width="1670.76"
          height="19.040000000000003"
        />
        <rect
          x="319.29"
          y="156.67"
          rx="0"
          ry="0"
          width="1543.6799999999998"
          height="791.3199999999999"
        />
        <rect
          x="365.29"
          y="103.67"
          rx="0"
          ry="0"
          width="184.23999999999998"
          height="39.75"
        />
        <rect x="673.29" y="152.67" rx="0" ry="0" width="0" height="0" />
        <rect
          x="324.29"
          y="110.77"
          rx="0"
          ry="0"
          width="27.44"
          height="25.439999999999998"
        />
      </ContentLoader>
    );
    const { children } = this.props;
    const { isLoading } = this.state;
    return isLoading ? (
      <div className="w-100">
        <AuthLoading />
      </div>
    ) : (
      children
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

export default withRouter(
  connect(
    mapStateToProps,
    { setUser: userActionCreators.set }
  )(AuthChecker)
);
