import React, { Component } from "react";
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";
import PropTypes from "prop-types";

import {
  AppNavbarBrand,
  AppSidebarToggler,
  AppHeaderDropdown
} from "@coreui/react";
import logo from "../../assets/img/brand/logo.png";
import sygnet from "../../assets/img/brand/sygnet.png";
import userImg from "../../assets/img/user.png";
import { actionCreators as userActions } from "../../reducer/user";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {
    const { user } = this.props;
    return !user.token ? (
      <Redirect to="/login" />
    ) : (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{
            src: logo,
            width: 140,
            height: 130,
            alt: "Tenemosnoticias.com"
          }}
          minimized={{
            src: sygnet,
            width: 65,
            height: 65,
            alt: "Tenemosnoticias.com"
          }}
        />

        <Nav className="d-md-down-none mr-4" navbar>
          <NavItem className="">
            <NavLink>Hola, {this.props.user.nombre}</NavLink>
          </NavItem>
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img
                src={user.imgUrl ? user.imgUrl : userImg}
                className="img-avatar"
                width="40px"
                height="40px"
                alt=""
              />
              <i className="icon-menu" />
            </DropdownToggle>
            <DropdownMenu right style={{ right: "auto" }}>
              <DropdownItem header tag="div" className="text-center">
                <strong>Acciones</strong>
              </DropdownItem>
              <DropdownItem onClick={this.props.resetUser}>
                <i className="fa fa-lock" /> Cerrar Sesión
              </DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

const mapStateToProps = state => ({
  user: state.user
});

const bindActionCreators = {
  resetUser: userActions.reset
};

export default connect(
  mapStateToProps,
  bindActionCreators
)(DefaultHeader);
