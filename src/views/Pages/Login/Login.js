import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from "reactstrap";
import api from "../../../api";
import { actionCreators as userActions } from "../../../reducer/user";
import { connect } from "react-redux";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Redirect } from "react-router-dom";
import Alert from "react-s-alert";
import LoadingSpinner from "../../../assets/img/oval.svg";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(email, password) {
    this.setState({
      isLoading: true
    });
    this.loginRequestForm(email, password);
  }

  loginRequestForm = async ({ email, password }) => {
    const { error, data, message } = await api.login({
      email: email,
      password: password
    });
    if (error) {
      this.setState({
        isLoading: false
      });
      Alert.error(message, {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
    }

    if (data) {
      this.setState({
        isLoading: false
      });
      const user = data.data;
      this.props.setUser(user);
    }
  };

  render() {
    const { user } = this.props;
    return user.token ? (
      <Redirect to="/panel/editar" />
    ) : (
      <div className="app flex-row align-items-center animated fadeIn">
        <Container>
          <Row className="justify-content-center">
            <Alert stack={{ limit: 1 }} />
            <Col md="8">
              <Card className="p-4">
                <CardBody>
                  <Formik
                    initialValues={{ email: "", password: "" }}
                    onSubmit={this.handleClick}
                    validationSchema={Yup.object().shape({
                      email: Yup.string()
                        .email("Debes ingresar un email valido")
                        .required("Debe ingresar un email"),
                      password: Yup.string().required(
                        "Debe ingresar una contraseña"
                      )
                    })}
                    enableReinitialize={true}
                    render={({ errors, touched, dirty }) => (
                      <Form
                        method="post"
                        encType="multipart/form-data"
                        className="form-horizontal"
                      >
                        <h2>Panel Administrativo</h2>
                        <p className="text-muted">
                          Para acceder debes utilizar tu cuenta de{" "}
                          <b>www.tenemosnoticias.com</b>
                        </p>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Field
                            type="text"
                            placeholder="Correo Electronico"
                            name="email"
                            className="form-control"
                          />
                        </InputGroup>
                        <small className="text-danger">
                          {touched.email && errors.email}
                        </small>
                        <InputGroup className="mt-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-lock" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Field
                            type="password"
                            placeholder="Contraseña"
                            name="password"
                            className="form-control"
                          />
                        </InputGroup>
                        <small className="text-danger">
                          {touched.password && errors.password}
                        </small>
                        <Row className="mt-4">
                          <Col xs="6">
                            <Button
                              type="submit"
                              color="tn"
                              className="px-4"
                              disabled={!dirty}
                            >
                              {this.state.isLoading ? (
                                <img
                                  src={LoadingSpinner}
                                  alt="Cargando..."
                                  width="20px"
                                />
                              ) : (
                                "Entrar"
                              )}
                            </Button>
                          </Col>
                        </Row>
                      </Form>
                    )}
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const bindActionCreators = {
  setUser: userActions.set
};

export default connect(
  mapStateToProps,
  bindActionCreators
)(Login);
