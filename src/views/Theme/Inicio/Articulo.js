import React, { Component } from "react";
import { Formik, Form, Field } from "formik";
// import * as Yup from "yup";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import {
  Card,
  CardBody,
  Modal,
  ModalFooter,
  ModalHeader,
  Label,
  Row,
  Col,
  Button,
  FormGroup,
  UncontrolledTooltip
} from "reactstrap";
import strftime from "strftime";
import Alert from "react-s-alert";
import api from "../../../api";

import ContentLoader from "react-content-loader";

class Articulo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tooltipOpen: false,
      isLoading: true,
      itsOff: false,
      itsOffArticle: false,
      danger: false,
      success: false,
      text: "",
      link: "",
      currentArticle: null,
      currentCategories: null,
      currentFuentes: null,
      countFuentes: null,
      views: null,
      redirect: null,
      categories: [],
      result: [],
      articulosDesactivados: [],
      currentArticlesByFuentes: []
    };
    this.toggle = this.toggle.bind(this);
    this.toggleDanger = this.toggleDanger.bind(this);
    this.toggleSuccess = this.toggleSuccess.bind(this);
  }

  // instance = createRef();
  // processMedias = () => {
  //   const twitter = document.createElement("script");
  //   twitter.type = "text/javascript";
  //   twitter.async = true;
  //   twitter.defer = true;
  //   twitter.src = "https://platform.twitter.com/widgets.js";
  //   this.instance.current.appendChild(twitter);
  //
  //   const instagram = document.createElement("script");
  //   instagram.type = "text/javascript";
  //   instagram.async = true;
  //   instagram.defer = true;
  //   instagram.src = "https://platform.instagram.com/en_US/embeds.js";
  //   this.instance.current.appendChild(instagram);
  // }

  componentDidMount(props) {
    const slug = this.props.handleSlug;
    const id = this.props.handleId;
    this.fetchArticle(slug, id);
    this.fetchCategories();
  }

  componentDidUpdate(prevProps) {
    const slug = this.props.handleSlug;
    const id = this.props.handleId;
    if (prevProps.handleSlug !== slug || prevProps.handleId !== id) {
      this.fetchArticle(slug, id);
    }
    this._itsOffNotice();
    // this._itsOffArticle();
  }

  shouldComponentUpdate(nextState) {
    return nextState.itsOff !== this.state.itsOff;
  }

  fetchArticle = async (slug, id) => {
    const { error, data } = await api.getArticle(slug, id);
    if (error) {
      Alert.error("La noticia esta desactivada o no existe", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
    }

    if (data) {
      const articulo = data.articulosFuente;
      const noticias = data.noticia;
      const result = data;
      const elementsByArticulo = articulo[0].articulos;
      this.setState({
        isLoading: false,
        result,
        articulo,
        currentArticle: articulo[0].articulos[0],
        currentFuentes: articulo
          .filter(x => x.articulos[0].id !== articulo[0].articulos[0].id)
          .map(cantFuentes => (
            <div
              key={cantFuentes.articulos[0].id}
              defaultValue={`https://admin.tenemosnoticias.com/api/story/find-by-new/${
                this.props.handleSlug
              }/${cantFuentes.articulos[0].id}`}
              href="#"
              className="text-center"
              onClick={() => {
                this.props.history.push(
                  `/panel/editar/${this.props.handleSlug}/${
                    cantFuentes.articulos[0].id
                  }`
                );
              }}
            >
              <button
                className="btn btn-outline-dark btn-block mt-2"
                key={cantFuentes.fuente.id}
              >
                {this.Capitalize(cantFuentes.fuente.slug)}
              </button>
            </div>
          )),
        currentArticlesByFuentes: elementsByArticulo
          .filter(x => x.id !== articulo[0].articulos[0].id)
          .map(articulos => (
            <button
              className="btn btn-outline-dark btn-block mt-2 white-space"
              key={articulos.id}
              onClick={() => {
                this.props.history.push(
                  `/panel/editar/${this.props.handleSlug}/${articulos.id}`
                );
              }}
            >
              {articulos.titulo}
            </button>
          )),
        getArticleSlugByFuentes: noticias.slug,
        currentCategorie: noticias.categoria,
        views: noticias.vistas,
        noticiaId: noticias.id

        // mapImagenes: (_,i) => `<img src="${articulo[0].articulos[0].imagenes[i].dimensionesDisponibles[1].url}" width="100%"/>`,
        // mapVideos: (_, i) => `<iframe src="${articulo[0].articulos[0].videos[i].url}" width="100%" ></iframe>`,
        // mapTweet: (_, i) => `<blockquote class="twitter-tweet" data-lang="es"><a href='${articulo[0].articulos[0].tweets[i].url}'></a></blockquote>`,
        // mapInstagram: (_, i) => `<blockquote class="instagram-media wc" data-instgrm-version="7" data-instgrm-captioned><a href='${articulo[0].articulos[0].instagram[i].url}'></a></blockquote>`,
      });
    }
  };
  fetchCategories = async () => {
    const { error, data } = await api.getCategories();
    if (data) {
      const currentCategories = data;
      this.setState({
        currentCategories
      });
    }

    if (error) {
      Alert.error("Hubo un problema al procesar la edicion de la noticia.", {
        position: "bottom-right",
        effect: "slide",
        timeout: 8000,
        offset: 30
      });
      console.log({ error });
    }
  };
  fetchUpdateArticle = async values => {
    // console.log(values);
    // console.log(this.state.articulosDesactivados);

    if (this.state.articulosDesactivados.length > 0) {
      this.state.articulosDesactivados.forEach(desactivada => {
        const articuloFuente = values.articulosFuente.find(
          af => af.id === desactivada.afId
        );

        const articulosFiltrados = articuloFuente.articulos.filter(
          articulo => articulo.id !== desactivada.articleId
        );

        if (articulosFiltrados.length === 0) {
          values.articulosFuente = values.articulosFuente.filter(
            af => af.id !== desactivada.afId
          );
        } else {
          articuloFuente.articulos = articulosFiltrados;
        }
      });
    }

    const { error, data } = await api.updateArticle({
      data: values,
      articulosDesactivados: this.state.articulosDesactivados
    });
    if (error) {
      console.log({ error });
      Alert.error("Hubo un error al actualizar la noticia", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
    }

    if (data) {
      Alert.success("¡Noticia actualizada exitosamente!", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
      setTimeout(() => window.location.reload(), 1000);
    }
  };
  fetchDesactivarNoticia = async () => {
    const { getArticleSlugByFuentes } = this.state;
    const { error } = await api.isNotActiveNotice(getArticleSlugByFuentes);

    if (error) {
      this.setState({
        danger: !this.state.danger
      });
      Alert.error("Esta noticia ya esta desactivada ", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
    } else {
      this.setState({
        danger: !this.state.danger,
        itsOff: true
      });
      Alert.success("¡Noticia Desactivada!", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
    }
  };
  fetchActivarNoticia = async () => {
    const { getArticleSlugByFuentes } = this.state;
    const { error } = await api.isActiveNotice(getArticleSlugByFuentes);
    if (error) {
      this.setState({
        success: !this.state.success
      });
      Alert.error("Esta noticia ya esta desactivada ", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
    } else {
      this.setState({
        success: !this.state.success,
        itsOff: false
      });
      Alert.success("¡Noticia Activada!", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
    }
  };
  fetchDesactivarArticulo = () => {
    const { result } = this.state;
    const afId = result.articulosFuente[0].id;
    const articleId = result.articulosFuente[0].articulos[0].id;
    this.setState(state => ({
      articulosDesactivados: [
        ...state.articulosDesactivados,
        { afId, articleId }
      ]
    }));
  };
  fetchActivarArticulo = id => () => {
    this.setState(state => ({
      articulosDesactivados: state.articulosDesactivados.filter(
        x => x.articleId !== id
      )
    }));
  };

  _itsOffNotice() {
    const { itsOff } = this.state;
    if (!itsOff) {
      return (
        <div className="card w-100 mt-4">
          <div className="card-header">
            <i className="icon-close" />
            Desactivar noticia
          </div>
          <div className="card-body">
            <button
              className="btn btn-block mt-2 btn-danger"
              onClick={this.toggleDanger}
            >
              Desactivar
            </button>
          </div>
        </div>
      );
    } else {
      return (
        <div className="card w-100 mt-4">
          <div className="card-header">
            <i className="icon-close" />
            Activar noticia
          </div>
          <div className="card-body">
            <button
              className="btn btn-block mt-2 btn-success"
              onClick={this.toggleSuccess}
            >
              Activar
            </button>
          </div>
        </div>
      );
    }
  }

  _itsOffArticle(id) {
    const { articulosDesactivados, currentFuentes } = this.state;
    // console.log(currentFuentes);
    if (currentFuentes.length >= 1 && currentFuentes.length !== 0) {
      if (!articulosDesactivados.some(x => x.articleId === id)) {
        return (
          <div className="text-right">
            <button
              className="btn btn-outline-danger btn-sm"
              onClick={this.fetchDesactivarArticulo}
            >
              Desactivar
            </button>
          </div>
        );
      } else {
        return (
          <div className="text-right">
            <button
              className="btn btn-outline-success btn-sm"
              onClick={this.fetchActivarArticulo(id)}
            >
              Activar
            </button>
          </div>
        );
      }
    } else {
      return (
        <div className="text-right">
          <div
            id="UncontrolledTooltipExample"
            className="btn btn-outline-danger"
          >
            Atención <i className="icon-arrow-right" />
          </div>
          <UncontrolledTooltip
            placement="right"
            target="UncontrolledTooltipExample"
          >
            Este es el último artículo de esta noticia. Si lo quiere desactivar,
            debe desactivar la noticia.
          </UncontrolledTooltip>
        </div>
      );
    }
  }

  Capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

  toggleDanger() {
    this.setState({
      danger: !this.state.danger
    });
  }

  toggleSuccess() {
    this.setState({
      success: !this.state.success
    });
  }

  render() {
    const es_MX = {
      months: [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
      ]
    };
    const strftimeES = strftime.localize(es_MX);

    const {
      currentCategories,
      currentArticle,
      currentFuentes,
      currentArticlesByFuentes,
      // mapImagenes,
      // mapTweet,
      // mapInstagram,
      // mapVideos,
      views,
      result,
      isLoading
    } = this.state;

    const ArticleLoadingComponent = props => (
      <ContentLoader
        height={1000}
        width={1610}
        speed={2}
        primaryColor="#eaebec"
        secondaryColor="#dad8d8"
        {...props}
      >
        <rect x="21" y="18.03" rx="0" ry="0" width="372.19" height="94.53" />
        <rect x="1101" y="99.61" rx="0" ry="0" width="0" height="0" />
        <rect x="409" y="19.03" rx="0" ry="0" width="383.24" height="94.53" />
        <rect x="808" y="19.03" rx="0" ry="0" width="383.24" height="94.53" />
        <rect x="1208" y="19.03" rx="0" ry="0" width="383.24" height="94.53" />
        <rect x="30.62" y="139.27" rx="0" ry="0" width="35.34" height="32.55" />
        <rect x="72.63" y="143.27" rx="0" ry="0" width="98.98" height="14.62" />
        <rect x="28.63" y="220.27" rx="0" ry="0" width="80" height="15" />
        <rect x="28.63" y="280.27" rx="0" ry="0" width="100.8" height="15" />
        <rect x="287.63" y="216.27" rx="0" ry="0" width="502.08" height="17" />
        <rect
          x="288.63"
          y="280.27"
          rx="0"
          ry="0"
          width="805.42"
          height="598.06"
        />
        <rect
          x="1248.63"
          y="214.03"
          rx="0"
          ry="0"
          width="327.97"
          height="36.52"
        />
        <rect
          x="1267.63"
          y="159.27"
          rx="0"
          ry="0"
          width="98.98"
          height="14.62"
        />
        <rect
          x="1222.62"
          y="156.27"
          rx="0"
          ry="0"
          width="35.34"
          height="32.55"
        />
        <rect
          x="1248.63"
          y="256.03"
          rx="0"
          ry="0"
          width="327.97"
          height="36.52"
        />
        <rect
          x="1248.63"
          y="299.03"
          rx="0"
          ry="0"
          width="327.97"
          height="36.52"
        />
        <rect
          x="1269.63"
          y="398.27"
          rx="0"
          ry="0"
          width="98.98"
          height="14.62"
        />
        <rect
          x="1226.62"
          y="394.27"
          rx="0"
          ry="0"
          width="35.34"
          height="32.55"
        />
        <rect
          x="1248.63"
          y="443.03"
          rx="0"
          ry="0"
          width="327.97"
          height="36.52"
        />
        <rect x="27.63" y="919.27" rx="0" ry="0" width="83.66" height="15" />
        <rect x="288.63" y="917.27" rx="0" ry="0" width="180.75" height="17" />
      </ContentLoader>
    );

    return (
      <div className="animated fade-in" ref={this.instance}>
        <Alert stack={{ limit: 1 }} />
        {!isLoading && result.articulosFuente ? (
          <Row className="mt-4 mb-5">
            <Col xs="12" sm="6" lg="3">
              <Card className="text-white bg-info">
                <CardBody className="pb-4">
                  <div className="text-value">
                    <i className="icon-book-open" /> Fuente
                  </div>
                  <div>
                    {currentArticle.fuentePorArticulo.fuente.nombreCorto}
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col xs="12" sm="6" lg="3">
              <Card className="text-white bg-success">
                <CardBody className="pb-4">
                  <div className="text-value">
                    <i className="icon-calendar" /> Fecha
                  </div>
                  {strftimeES(
                    "%B %d, %Y",
                    new Date(currentArticle.fechaPublicacion)
                  )}
                </CardBody>
              </Card>
            </Col>
            <Col xs="12" sm="6" lg="3">
              <Card className="text-white bg-warning">
                <CardBody className="pb-4">
                  <div className="text-value">
                    <i className="icon-clock" /> Publicado hace
                  </div>
                  <div>{currentArticle.timeAgo}</div>
                </CardBody>
              </Card>
            </Col>
            <Col xs="12" sm="6" lg="3">
              <Card className="text-white bg-danger">
                <CardBody className="pb-4">
                  <div className="text-value">
                    <i className="icon-eye" /> Vistas
                  </div>
                  <div>{views}</div>
                </CardBody>
              </Card>
            </Col>
            <Col xs="12" md="8">
              <Formik
                initialValues={this.state.result}
                onSubmit={this.fetchUpdateArticle}
                enableReinitialize
                render={({ setFieldValue, values }) => (
                  <div className="card w-100 m-auto">
                    <div className="card-header">
                      <div className="row">
                        <div className="col-lg-8 col-sm-7 col-7">
                          <i className="icon-book-open" /> Artículo
                        </div>
                        <div className="col-lg-4 col-sm-5 col-5">
                          {this._itsOffArticle(
                            this.state.result.articulosFuente[0].articulos[0].id
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <Form
                        action=""
                        method="post"
                        encType="multipart/form-data"
                        className="form-horizontal"
                      >
                        <FormGroup row>
                          <Col md="3">
                            <Label>Título</Label>
                          </Col>
                          <Col xs="12" md="9">
                            {!this.state.itsOff ? (
                              <Field
                                type="text"
                                name="articulosFuente[0].articulos[0].titulo"
                                placeholder="Título de la noticia"
                                className="form-control article-title"
                                value={
                                  values.articulosFuente[0].articulos[0].titulo
                                }
                              />
                            ) : (
                              <Field
                                type="text"
                                name="articulosFuente[0].articulos[0].titulo"
                                placeholder="Titulo de la noticia"
                                className="form-control article-title"
                                value={
                                  values.articulosFuente[0].articulos[0].titulo
                                }
                                disabled
                              />
                            )}
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label>Contenido</Label>
                          </Col>
                          <Col xs="12" md="9">
                            {!this.state.itsOff ? (
                              <ReactQuill
                                value={
                                  values.articulosFuente[0].articulos[0]
                                    .contenido
                                  // .replace(/2AE8rLFJHEriDjHSss9Cb8(\d+)/g, mapImagenes)
                                  // .replace(/MLvx2ELuRs8P4QL3WtdE3k(\d+)/g, mapTweet)
                                  // .replace(/VxxjRDwrsmuDHAJwaGyJRe(\d+)/g, mapVideos)
                                  // .replace(/AVhyEdHYXoWdTqwa3kGGxa(\d+)/g, mapInstagram)
                                }
                                onChange={x =>
                                  setFieldValue(
                                    "articulosFuente[0].articulos[0].contenido",
                                    x
                                  )
                                }
                              />
                            ) : (
                              <ReactQuill
                                value={
                                  values.articulosFuente[0].articulos[0]
                                    .contenido
                                }
                                readOnly={true}
                              />
                            )}
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label>Categorías</Label>
                          </Col>
                          <Col xs="12" md="9">
                            {!this.state.itsOff ? (
                              <Field
                                component="select"
                                name="noticia.categoria.id"
                                className="form-control category-title"
                              >
                                {currentCategories.map(categorie => (
                                  <option
                                    key={categorie.id}
                                    value={categorie.id}
                                  >
                                    {this.Capitalize(
                                      categorie.slug.replace(/-/g, " ")
                                    )}
                                  </option>
                                ))}
                              </Field>
                            ) : (
                              <Field
                                component="select"
                                name="noticia.categoria.id"
                                className="form-control"
                                disabled
                              >
                                {currentCategories.map(categorie => (
                                  <option
                                    key={categorie.id}
                                    value={categorie.id}
                                  >
                                    {this.Capitalize(
                                      categorie.slug.replace(/-/g, " ")
                                    )}
                                  </option>
                                ))}
                              </Field>
                            )}
                          </Col>
                        </FormGroup>
                        <div className="card-footer">
                          <div className="form-actions text-right">
                            {!this.state.itsOff ? (
                              <Button
                                type="submit"
                                color="tn"
                                className="btn-pill"
                              >
                                <i className="icon-cloud-download" /> Guardar
                                Cambios
                              </Button>
                            ) : (
                              <Button
                                type="submit"
                                color="tn"
                                className="btn-pill"
                                disabled
                              >
                                <i className="icon-cloud-download" /> Guardar
                                Cambios
                              </Button>
                            )}
                          </div>
                        </div>
                      </Form>
                    </div>
                  </div>
                )}
              />
            </Col>
            <Col xs="12" md="4" className="otros-articulos">
              {currentArticlesByFuentes.length !== 0 && (
                <div className="card w-100">
                  <div className="card-header">
                    <i className="icon-notebook" />
                    Otros articulos
                  </div>
                  <div className="card-body">{currentArticlesByFuentes}</div>
                </div>
              )}
              {currentFuentes.length !== 0 && (
                <div className="card w-100 m-auto">
                  <div className="card-header">
                    <i className="icon-feed" /> Otras fuentes
                  </div>
                  <div className="card-body">{currentFuentes}</div>
                </div>
              )}
              {this._itsOffNotice()}
            </Col>
          </Row>
        ) : (
          <div className="w-100">
            <ArticleLoadingComponent />
          </div>
        )}

        <Modal
          isOpen={this.state.danger}
          toggle={this.toggleDanger}
          className={"modal-danger " + this.props.className}
        >
          <ModalHeader toggle={this.toggleDanger}>
            ¿Realmente desea desactivar esta noticia?
          </ModalHeader>
          <ModalFooter>
            <Button color="secondary" onClick={this.fetchDesactivarNoticia}>
              Desactivar
            </Button>
            <Button color="danger" onClick={this.toggleDanger}>
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.success}
          toggle={this.toggleSuccess}
          className={"modal-success " + this.props.className}
        >
          <ModalHeader toggle={this.toggleSuccess}>
            ¿Realmente desea activar esta noticia?
          </ModalHeader>
          <ModalFooter>
            <Button color="secondary" onClick={this.fetchActivarNoticia}>
              Activar
            </Button>
            <Button color="success" onClick={this.toggleSuccess}>
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default Articulo;
