import React, { Component } from "react";
import {
  Col,
  Button,
  FormGroup,
  Alert,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane
} from "reactstrap";
import classnames from "classnames";
import "react-quill/dist/quill.snow.css";
import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/slide.css";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import Articulo from "./Articulo";
import Busqueda from "./Busqueda";
import { Route } from "react-router-dom";
import { connect } from "react-redux";

// const articleRegex = /(http?:\/\/)?192\.168\.0\.23:3000\/story\/find-by-new\/(.+?)\/(.+?)\/*$/i;
const articleRegex = /(https?:\/\/)?(www\.)?tenemosnoticias\.com\/noticia\/(.+?)\/(.+?)\/*$/i;

class Colors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      link: "",
      linkSearch: "",
      slug: "",
      id: "",
      resultEdit: "",
      activeTab: "1",
      search: ""
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  componentDidMount() {
    const { slug, id, search } = this.props.match.params;

    if (slug && id) {
      this.setState({
        slug,
        id,
        link: `${slug}/${id}`,
        activeTab: "2"
      });
    }

    if (search) {
      this.setState({
        linkSearch: search,
        activeTab: "1"
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { slug: oldSlug, id: oldId } = prevProps.match.params;
    const { slug, id } = this.props.match.params;
    const { linkSearch: oldlinkSearch } = prevProps.match.params;
    const { linkSearch } = this.props.match.params;

    if (oldSlug !== slug || oldId !== id) {
      this.setState({
        slug,
        id,
        link: `${slug}/${id}`
      });
    }

    if (oldlinkSearch !== linkSearch) {
      this.setState({
        linkSearch
      });
    }
  }

  getLink = ({ link }) => {
    try {
      const findData = link.match(articleRegex);
      let FIND_SLUG = findData[3];
      let FIND_ID = findData[4];

      return this.setState(
        {
          link: link,
          slug: FIND_SLUG,
          id: FIND_ID,
          activeTab: "2"
        },
        () => this.props.history.push(`/panel/editar/${FIND_SLUG}/${FIND_ID}`)
      );
    } catch (e) {}
  };

  getSearchKey = ({ searchKey }) => {
    const searchFilter = searchKey.replace(/ /g, "-").toLowerCase();
    const url = searchFilter;

    return this.setState(
      {
        linkSearch: url,
        search: searchFilter
      },
      () => this.props.history.push(`/panel/resultado/${this.state.search}`)
    );
  };

  render() {
    const { linkSearch, slug, id } = this.state;
    let resultEdit;
    let resultSearch;

    if (slug !== "" && id !== "") {
      resultEdit = (
        <Route
          path={`/panel/editar/:slug/:id`}
          render={props => (
            <Articulo {...props} handleSlug={slug} handleId={id} />
          )}
        />
      );
    } else {
      resultEdit = (
        <Alert color="danger">
          <i className="icon-info" /> Aún no ha seleccionado <b>ninguna</b>{" "}
          noticia.
        </Alert>
      );
    }

    if (linkSearch !== "") {
      resultSearch = (
        <div>
          <span className="font-2xl font-weight-bold">Resultados:</span>
          <Route
            path={`/panel/resultado/:search`}
            render={props => (
              <Busqueda {...props} handleLinkSearch={linkSearch} />
            )}
          />
        </div>
      );
    }

    return (
      <div className="animated fadeIn">
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === "1" })}
              onClick={() => {
                this.toggle("1");
              }}
            >
              <i className="icon-magnifier" />{" "}
              <span className={this.state.activeTab === "1" ? "" : "d-none"}>
                {" "}
                Buscar Noticia
              </span>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === "2" })}
              onClick={() => {
                this.toggle("2");
              }}
            >
              <i className="icon-note" />{" "}
              <span className={this.state.activeTab === "2" ? "" : "d-none"}>
                {" "}
                Editar Noticia{" "}
              </span>
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col xs="12" md="12">
                <Formik
                  initialValues={{ searchKey: "" }}
                  onSubmit={this.getSearchKey}
                  validationSchema={Yup.object().shape({
                    searchKey: Yup.string().required(
                      "Debe ingresar alguna palabra para poder realizar la búsqueda"
                    )
                  })}
                  render={({ errors, touched, dirty }) => (
                    <div className="card">
                      <Form>
                        <div className="card-body">
                          <p>
                            Ingrese alguna <b>palabra clave</b> o el{" "}
                            <b>título</b> de la noticia que desee buscar.
                          </p>
                          <FormGroup row>
                            <Col md="10" sm="12">
                              <Field
                                type="text"
                                name="searchKey"
                                placeholder="Encuentra una noticia..."
                                className="form-control"
                              />
                              <small className="text-danger">
                                {touched.searchKey && errors.searchKey}
                              </small>
                            </Col>
                            <Col md="2" sm="12">
                              <Button
                                type="submit"
                                color="dark"
                                className="btn-pill w-100 buscar"
                                disabled={!dirty}
                              >
                                <i className="icon-magnifier" /> Buscar
                              </Button>
                            </Col>
                          </FormGroup>
                        </div>
                      </Form>
                    </div>
                  )}
                />
              </Col>
              <div className="loader">
                <div className="icon" />
              </div>
            </Row>
            {resultSearch}
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col xs="12" md="12">
                <Formik
                  initialValues={{ link: "" }}
                  onSubmit={this.getLink}
                  validationSchema={Yup.object().shape({
                    link: Yup.string()
                      .required("Debe ingresar una URL para continuar")
                      .matches(
                        articleRegex,
                        "El link que acabas de ingresar no es valido"
                      )
                  })}
                  render={({ errors, touched, dirty }) => (
                    <div className="card">
                      <Form>
                        <div className="card-body">
                          <p>
                            Ingrese la Url de la noticia que desea <b>editar</b>
                          </p>
                          <FormGroup row>
                            <Col xs="12">
                              <Field
                                type="text"
                                name="link"
                                placeholder="Ingrese una URL"
                                className="form-control"
                              />
                              <small className="text-danger">
                                {touched.link && errors.link}
                              </small>
                            </Col>
                          </FormGroup>
                        </div>
                        <div className="card-footer">
                          <div className="form-actions">
                            <Button
                              type="submit"
                              color="tn"
                              className="btn-pill"
                              disabled={!dirty}
                            >
                              <i className="icon-note" /> Editar
                            </Button>
                          </div>
                        </div>
                      </Form>
                    </div>
                  )}
                />
              </Col>
              <div className="loader">
                <div className="icon" />
              </div>
            </Row>
            {resultEdit}
          </TabPane>
        </TabContent>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps)(Colors);
