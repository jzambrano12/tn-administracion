import React, { Component } from "react";
import Alert from "react-s-alert";
import {
  Row,
  Col,
  Badge,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Card,
  CardImg,
  CardTitle,
  CardText,
  CardDeck,
  CardSubtitle,
  CardBody
} from "reactstrap";
import noNews from "../../../assets/img/nonews.png";
import strftime from "strftime";
import ContentLoader from "react-content-loader";
import api from "../../../api";
import ImagenDefault from "../../../assets/img/baking.png";

class Busqueda extends Component {
  constructor(props) {
    super(props);
    this.state = {
      linkSearch: "",
      searchData: "",
      danger: "",
      slug: "",
      tituloArticulo: "",
      imagenArticulo: "",
      slugArticulo: "",
      imgArticle: [],
      err500: null,
      isLoading: true
    };
    this.toggleDanger = this.toggleDanger.bind(this);
  }

  componentDidMount(props) {
    const linkSearch = this.props.handleLinkSearch;
    this.setState({
      linkSearch
    });
    this.fetchSearching(linkSearch);
  }

  componentDidUpdate(prevProps, prevState) {
    const linkSearch = this.props.handleLinkSearch;
    if (prevProps.handleLinkSearch !== linkSearch) {
      this.fetchSearching(linkSearch);
    }
  }

  shouldComponentUpdate(nextState) {
    return nextState.linkSearch !== this.state.linkSearch;
  }

  fetchSearching = async linkSearch => {
    const { error, data } = await api.getSearch(linkSearch);
    if (data) {
      this.setState({
        isLoading: false,
        searchData: data
      });

      Alert.success(
        this.state.searchData.length === 1
          ? this.state.searchData.length + " Noticia encontrada"
          : this.state.searchData.length + " Noticias encontradas",
        {
          position: "bottom-right",
          effect: "slide",
          timeout: 6000,
          offset: 30
        }
      );
    }
    if (error) {
      Alert.error("Hubo un problema al procesar la busqueda", {
        position: "bottom-right",
        effect: "slide",
        timeout: 6000,
        offset: 30
      });
      this.setState({
        isLoading: false
      });
      console.log({ error });
    }
  };

  toggleDanger(titulo, slug, imagen) {
    this.setState({
      danger: !this.state.danger,
      tituloArticulo: titulo,
      slugArticulo: slug,
      imagenArticulo: imagen
    });
  }

  fetchDesactivar = async slug => {
    const { error } = await api.isNotActiveNotice(slug);

    if (error) {
      this.setState({
        danger: !this.state.danger
      });
      Alert.error("Esta noticia ya esta desactivada ", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
    } else {
      this.setState({
        danger: !this.state.danger
      });
      Alert.success("Noticia desactivada", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });

      const linkSearch = this.props.handleLinkSearch;
      setTimeout(() => this.fetchSearching(linkSearch), 1000);
    }
  };

  getSearchData() {
    const { searchData } = this.state;
    const es_MX = {
      months: [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
      ]
    };
    const strftimeES = strftime.localize(es_MX);
    if (searchData.length !== 0) {
      return searchData.map(current => (
        <Col xs="12" md="12" className="mt-3 mb-3" key={current.noticia.id}>
          <CardDeck>
            <Card className="card-remove">
              {current.articulosFuente[0].articulos[0].imagenes[0] ? (
                <CardImg
                  top
                  className="card-img-dimensions"
                  src={
                    current.articulosFuente[0].articulos[0].imagenes[0]
                      .dimensionesDisponibles.length !== 0 &&
                    current.articulosFuente[0].articulos[0].imagenes[0]
                      .dimensionesDisponibles.length > 0
                      ? current.articulosFuente[0].articulos[0].imagenes[0]
                          .dimensionesDisponibles[0].url
                      : ImagenDefault
                  }
                  alt={current.noticia.titulo}
                />
              ) : (
                <CardImg
                  top
                  className="card-img-dimensions"
                  src={ImagenDefault}
                  alt={current.noticia.titulo}
                />
              )}
              <CardBody>
                <Row>
                  <Col md="12" className="status-article">
                    <CardSubtitle>
                      {current.noticia.categoria.nombre}
                    </CardSubtitle>
                    <CardText>
                      {strftimeES(
                        "%B %d, %Y",
                        new Date(current.noticia.fechaPublicacion)
                      )}
                    </CardText>
                  </Col>
                </Row>
                <Row>
                  <Col md="12" className="title-article mt-4">
                    <CardTitle>{current.noticia.titulo}</CardTitle>
                  </Col>
                </Row>
                <Row>
                  <Col md="12" className="interaction-article">
                    <CardText>
                      <Badge color="light" className="p-2 mr-3">
                        <i className="icon-eye font-2xl" />{" "}
                        <span className="views-card">
                          {current.noticia.vistas}
                        </span>
                      </Badge>
                      <Badge color="secondary" className="p-2">
                        <i className="icon-share-alt font-2xl" />{" "}
                        <span className="views-card">
                          {current.noticia.compartidos}
                        </span>
                      </Badge>
                    </CardText>
                  </Col>
                </Row>
                <Row className="mt-4">
                  <Col md="12" className="text-right">
                    <div className="button-align">
                      <Button
                        color="dark"
                        className="btn-pill mr-3"
                        onClick={() =>
                          this.props.history.push(
                            `/panel/editar/${current.noticia.slug}/${
                              current.articulosFuente[0].articulos[0].id
                            }/`
                          )
                        }
                      >
                        <i className="icon-pencil" /> Editar
                      </Button>
                      <Button
                        color="danger"
                        className="btn-pill"
                        title="Desactivar noticia"
                        onClick={() =>
                          this.toggleDanger(
                            current.noticia.titulo,
                            current.noticia.slug,
                            current.articulosFuente[0].articulos[0].imagenes[0]
                              .dimensionesDisponibles[1].url
                          )
                        }
                      >
                        <i className="icon-close" /> Desactivar
                      </Button>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </CardDeck>
        </Col>
      ));
    } else {
      return (
        <div className="w-100 text-center">
          <img
            src={noNews}
            className="no-news"
            alt="No se encontraron noticias"
            width="640px"
            height="512px"
          />
        </div>
      );
    }
  }

  render() {
    const { searchData, isLoading } = this.state;
    const SearchLoadingComponent = props => (
      <ContentLoader
        height={254}
        width={1610}
        speed={2}
        primaryColor="#eaebec"
        secondaryColor="#dad8d8"
        {...props}
      >
        <rect x="21" y="18.03" rx="0" ry="0" width="368.5" height="214.83" />
        <rect x="407" y="19.02" rx="0" ry="0" width="69.93" height="8.06" />
        <rect x="407.08" y="33.79" rx="0" ry="0" width="90.65" height="8.28" />
        <rect
          x="407.36"
          y="62.61"
          rx="0"
          ry="0"
          width="1185.84"
          height="22.09"
        />
        <rect x="1101" y="99.61" rx="0" ry="0" width="0" height="0" />
        <rect
          x="407.36"
          y="89.61"
          rx="0"
          ry="0"
          width="483.12"
          height="22.09"
        />
        <rect x="407.36" y="124.61" rx="0" ry="0" width="36.6" height="30.08" />
        <rect x="453.36" y="124.61" rx="0" ry="0" width="36.6" height="30.08" />
        <rect
          x="1494.36"
          y="179.98"
          rx="0"
          ry="0"
          width="95.16"
          height="28.67"
        />
        <rect x="1413" y="179.98" rx="0" ry="0" width="73.2" height="29.61" />
      </ContentLoader>
    );
    return (
      <div className="animated fadeIn">
        <Alert stack={{ limit: 1 }} />
        <Row>
          {!isLoading && searchData ? (
            this.getSearchData()
          ) : (
            <div className="w-100">
              <SearchLoadingComponent />
              <SearchLoadingComponent />
              <SearchLoadingComponent />
            </div>
          )}

          <Modal
            isOpen={this.state.danger}
            toggle={() => this.toggleDanger()}
            className={"modal-danger " + this.props.className}
          >
            <ModalHeader toggle={() => this.toggleDanger()}>
              ¿Realmente desea desactivar esta noticia?
            </ModalHeader>
            <ModalBody className="text-center">
              <div className="w-75 m-auto">
                <img
                  src={this.state.imagenArticulo}
                  alt={this.state.tituloArticulo}
                  width="85%"
                />
                <h3 className="mt-2">{this.state.tituloArticulo}</h3>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="secondary"
                onClick={() => this.fetchDesactivar(this.state.slugArticulo)}
              >
                Desactivar
              </Button>
              <Button color="danger" onClick={() => this.toggleDanger()}>
                Cancelar
              </Button>
            </ModalFooter>
          </Modal>
        </Row>
      </div>
    );
  }
}

export default Busqueda;
