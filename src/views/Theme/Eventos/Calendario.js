import React from 'react';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
// import api from '../../../api';
import 'react-big-calendar/lib/css/react-big-calendar.css';
// import {ModalFooter, ModalBody, Modal, Button} from 'reactstrap';

let allViews = Object.keys(BigCalendar.Views).flatMap(k => BigCalendar.Views[k]);
moment.locale('es', {
  months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
  weekdaysShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom'],
  weekdays: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo']
});
const localizer = BigCalendar.momentLocalizer(moment);

const messages = {
  allDay: 'Todo el día',
  previous: '',
  next: '',
  today: 'Hoy',
  month: 'Mes',
  week: 'Semana',
  day: 'Día',
  agenda: 'Agenda',
  date: 'Fecha',
  time: 'Hora',
  event: 'Evento',
  showMore: total => `+ ${total} evento(s) adicional(es)`
};

const Calendario = ({events, toggle, history}) => (
    <BigCalendar
      messages={messages}
      localizer={localizer}
      events={events.map(e => ({id: e.id,title: e.titulo, start: new Date(e.fechaInicio), end: new Date(e.fechaFin), resource: e}))}
      startAccessor="start"
      endAccessor="end"
      views={allViews}
      step={60}
      defaultDate={new Date()}
      // onSelectEvent={({resource: {titulo, descripcion, fechaInicio, fechaFin, portada}}) => toggle(titulo, descripcion, fechaInicio, fechaFin, portada)}
      onSelectEvent={({resource: { slug }}) => history.push(`/panel/modificar-eventos/${slug}`)}
    />
  );

export default Calendario;
