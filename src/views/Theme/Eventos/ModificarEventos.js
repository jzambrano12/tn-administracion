import React, { Component } from "react";
import api from "../../../api";
import {
  Row,
  Col,
  FormGroup,
  Label,
  Button,
  UncontrolledTooltip
} from "reactstrap";
import ReactQuill from "react-quill";
import { Formik, Field, Form } from "formik";
import Dropzone from "react-dropzone";
import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/slide.css";
import "react-tagsinput/react-tagsinput.css";
import * as Yup from "yup";
import DateRangePicker from "@wojtekmaj/react-daterange-picker";
import strftime from "strftime";
import {
  arrayMove,
  SortableElement,
  SortableContainer
} from "react-sortable-hoc";
import LoadingSpinner from "../../../assets/img/oval.svg";
import Alert from "react-s-alert";

const gridStyles = {
  display: "grid",
  gridTemplateColumns: "repeat(2, 1fr)",
  gridGap: "30px",
  padding: "10px"
};

const gridItemStyles = {
  height: "230px",
  backgroundColor: "#e5e5e5"
};

const GridItem = SortableElement(({ value, onRemove }) => (
  <div
    className="delete-wrapper"
    style={{
      ...gridItemStyles,
      background: `url(${value.url}) no-repeat center / cover`
    }}
  >
    <div className="showDeleteIcon">
      <button className="delete-icon" onClick={onRemove} />
    </div>
  </div>
));

const Grid = SortableContainer(({ items = [], onRemove }) => (
  <div style={gridStyles}>
    {items.map((value, index) => (
      <GridItem
        key={`item-${index}`}
        index={index}
        value={value}
        onRemove={onRemove(index)}
      />
    ))}
  </div>
));

const handleImageFile = async imageFile => {
  const reader = new FileReader();
  const base64 = await new Promise((resolve, reject) => {
    reader.onerror = () => {
      reader.abort();
      reject();
    };

    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(imageFile);
  });

  const image = new Image();

  const preloadImage = () =>
    Image.prototype.decode
      ? image.decode()
      : new Promise((resolve, reject) => {
          image.onload = () => {
            console.info("Image successfully loaded");
            resolve();
          };
          image.onerror = () => {
            console.info("Error loading image");
            reject();
          };
        });

  image.src = base64;

  return await preloadImage().then(() => ({
    base64,
    width: image.width,
    height: image.height
  }));
};

class ModificarEventos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      items: [],
      currentNewEvents: {
        imagenes: [],
        fechaInicio: new Date(),
        fechaFin: new Date(),
        descripcion: "",
        titulo: "",
        tags: []
      },
      imagenes: [],
      tags: [],
      fechaInicio: new Date(),
      fechaFin: new Date(),
      descripcion: "",
      titulo: "",
      portada: ""
    };
  }

  componentDidMount() {
    const slug = this.props.match.params.newSlug;
    this.fetchEvents(slug);
  }

  componentDidUpdate(prevProps) {
    const slug = this.props.match.params.newSlug;
    if (prevProps.match.params.newSlug !== slug) {
      this.fetchEvents(slug);
    }
  }

  fetchEvents = async slug => {
    const { error, data } = await api.changeEvents(slug);
    if (error) {
      console.log(error);
    }

    if (data) {
      const es_MX = {
        months: [
          "Enero",
          "Febrero",
          "Marzo",
          "Abril",
          "Mayo",
          "Junio",
          "Julio",
          "Agosto",
          "Septiembre",
          "Octubre",
          "Noviembre",
          "Diciembre"
        ]
      };
      const strftimeES = strftime.localize(es_MX);
      const currentNewEvents = data;
      this.setState({
        currentNewEvents,
        fechaInicio: strftimeES(
          "%B %d, %Y",
          new Date(currentNewEvents.fechaInicio)
        ),
        fechaFin: strftimeES("%B %d, %Y", new Date(currentNewEvents.fechaFin))
        // tags: currentNewEvents.tags
      });
    }
  };

  handleChange(tags) {
    this.setState({ tags });
  }

  updateEvent = async values => {
    this.setState({
      isLoading: true
    });
    const imagenesEliminadas = this.state.currentNewEvents.imagenes.filter(
      i => !values.imagenes.find(i2 => i.id === i2.id)
    );
    const imagenesNuevas = values.imagenes.filter(i => !i.id);

    const { error, data } = await api.updateEvent({
      evento: {
        ...values,
        fechaInicio: values.fechaInicio.getTime(),
        fechaFin: values.fechaFin.getTime()
      },
      imagenesEliminadas:
        imagenesEliminadas.length > 0 ? imagenesEliminadas : undefined,
      imagenesNuevas: imagenesNuevas.length > 0 ? imagenesNuevas : undefined
    });

    if ({ error }) {
      this.setState({
        isLoading: false
      });
      Alert.error(`Hubo un error al actualizar el evento.`, {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
    }

    if (data) {
      this.setState({
        isLoading: false
      });
      Alert.success(`Evento actualizado`, {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
      this.props.history.push("/panel/ver-eventos")
    }
  };

  alertStack = { limit: 1 };

  render() {
    // const { tags } = this.state;
    return (
      <Row className="p-3 crear-eventos animated fadeIn h-100">
        <Alert stack={this.alertStack} />
        <Col xs="12">
          <Formik
            initialValues={{
              ...this.state.currentNewEvents,
              fechaInicio: new Date(this.state.currentNewEvents.fechaInicio),
              fechaFin: new Date(this.state.currentNewEvents.fechaFin)
            }}
            onSubmit={this.updateEvent}
            validationSchema={Yup.object().shape({
              descripcion: Yup.string().required(
                "Debe ingresar una descripción"
              ),
              titulo: Yup.string().required("Debe ingresar un título"),
              fechaInicio: Yup.date().typeError(
                "Ingrese una fecha de inicio valida"
              ),
              fechaFin: Yup.date().typeError("Ingrese una fecha fin valida")
            })}
            enableReinitialize
            render={({
              errors,
              touched,
              dirty,
              setFieldValue,
              values,
              setFieldTouched
            }) => (
              <Row className="p-4">
                <Form
                  className="crear-eventos-wrapper m-auto"
                  encType="multipart/form-data"
                >
                  <FormGroup row>
                    <Col xs="12" md="2">
                      <Label>Título</Label>
                    </Col>
                    <Col xs="12" md="10">
                      <Field
                        type="text"
                        name="titulo"
                        placeholder="Ingrese el título del evento"
                        className="form-control titulo-evento"
                        value={values.titulo}
                      />
                      <small className="text-danger">
                        {touched.titulo && errors.titulo}
                      </small>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col xs="12" md="2">
                      <Label>Descripción</Label>
                    </Col>
                    <Col xs="12" md="10">
                      <ReactQuill
                        value={values.descripcion}
                        onChange={x => setFieldValue("descripcion", x)}
                        placeholder="Descripción del evento"
                        onBlur={() => setFieldTouched("descripcion", true)}
                      />
                      <small className="text-danger">
                        {touched.descripcion && errors.descripcion}
                      </small>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col xs="12" md="2">
                      <Label>Fecha</Label>
                    </Col>
                    <Col xs="12" md="10">
                      <DateRangePicker
                        onChange={value => {
                          setFieldValue("fechaInicio", value && value[0]);
                          setFieldValue("fechaFin", value && value[1]);
                        }}
                          value={[values.fechaInicio, values.fechaFin]}
                      />
                      <br />
                      <small className="text-danger">
                        {touched.fechaInicio && errors.fechaInicio}
                      </small>
                      <br />
                      <small className="text-danger">
                        {touched.fechaFin && errors.fechaFin}
                      </small>
                    </Col>
                  </FormGroup>
                  {/*<FormGroup row>*/}
                  {/*<Col xs="12" md="2">*/}
                  {/*<Label>Tags</Label>*/}
                  {/*</Col>*/}
                  {/*<Col xs="12" md="10">*/}
                  {/*<TagsInput*/}
                  {/*value={tags}*/}
                  {/*onChange={x => setFieldValue("tags", x)}*/}
                  {/*/>*/}
                  {/*</Col>*/}
                  {/*</FormGroup>*/}
                  <FormGroup row>
                    <Col xs="12" md="2">
                      <Label>Imagen de portada</Label>
                    </Col>
                    <Col xs="12" md="10">
                      <input
                        type="file"
                        name="portada"
                        id="portada"
                        accept="image/png, image/jpeg"
                        onChange={async input => {
                          if (input.target.files && input.target.files[0]) {
                            const {
                              base64,
                              width,
                              height
                            } = await handleImageFile(input.target.files[0]);
                            if (width <= 1200 && height <= 1200) {
                              setFieldValue("portada", base64);
                            } else {
                              Alert.error(
                                `ERROR: La imagen es demasiado grande.`,
                                {
                                  position: "bottom-right",
                                  effect: "slide",
                                  timeout: 5000,
                                  offset: 30
                                }
                              );
                            }
                          }
                        }}
                      />{" "}
                      <br />
                      <div
                        className="view-events p-3 mt-2"
                        style={{
                          border: "1px solid",
                          width: "335px",
                          borderStyle: "dotted",
                          borderColor: "#ccc"
                        }}
                      >
                        <img
                          src={values.portada}
                          alt={values.titulo}
                          width="300px"
                        />
                      </div>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col xs="12" md="2">
                      <Label id="UncontrolledTooltipExample">
                        Otras imágenes{" "}
                        <span style={{ color: "red" }}>
                          <i className="icon-exclamation" />
                        </span>
                      </Label>
                      <UncontrolledTooltip
                        placement="bottom"
                        target="UncontrolledTooltipExample"
                      >
                        Para poder publicar un evento, el mismo debe tener al
                        menos una imagen. Las imagenes no deben ser mayor a
                        1200x1200
                      </UncontrolledTooltip>
                    </Col>
                    <Col xs="12" md="10">
                      <Dropzone
                        accept="image/jpeg, image/png"
                        onDropAccepted={async imageFiles => {
                          const imagesP = imageFiles.map(imageFile =>
                            handleImageFile(imageFile)
                          );
                          const images = await Promise.all(imagesP);
                          setFieldValue(
                            "imagenes",
                            (values.imagenes || []).concat(
                              images
                                .filter(
                                  ({ width, height }) =>
                                    width <= 1200 && height <= 1200
                                )
                                .map(image => ({ url: image.base64 }))
                            )
                          );
                          // images.forEach( =image> api.uploadPicture(image.url))
                        }}
                        className="dropzone-wrapper"
                      >
                        <Row>
                          <Col xs="12">
                            <div className="dropzone-icon">
                              <i className="icon-cloud-download" />
                            </div>
                          </Col>
                          <Col xs="12">
                            <div className="btn-dropzone-wrapper">
                              <span className="btn btn-tn">
                                Subir imagenes del evento
                              </span>
                              <p className="dropzone-text">
                                ... o también puedes arrastrarla
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </Dropzone>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col xs="12" md="2" />
                    <Col xs="12" md="10">
                      <Grid
                        onRemove={index => () =>
                          setFieldValue(
                            "imagenes",
                            values.imagenes.filter((_, i) => i !== index)
                          )}
                        items={values.imagenes}
                        onSortEnd={({ oldIndex, newIndex }) =>
                          setFieldValue(
                            "imagenes",
                            arrayMove(values.imagenes, oldIndex, newIndex)
                          )
                        }
                        axis="xy"
                        className="grid-wrapp"
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2" />
                    <Col md="10" className="p-0 text-right">
                      <Button type="submit" color="tn" className="btn-pill">
                        {this.state.isLoading ? (
                          <img
                            src={LoadingSpinner}
                            className="spinner"
                            alt="Cargando..."
                            width="20px"
                          />
                        ) : (
                          `Guardar Cambios`
                        )}
                      </Button>
                    </Col>
                  </FormGroup>
                </Form>
              </Row>
            )}
          />
        </Col>
      </Row>
    );
  }
}

export default ModificarEventos;
