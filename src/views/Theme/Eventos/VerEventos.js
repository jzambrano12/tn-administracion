import React, {Component, Fragment} from "react";
import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/slide.css";
import api from "../../../api";
import {
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
  CardFooter,
  Button,
  Row,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  Badge
} from "reactstrap";
import strftime from "strftime";
import Calendario from "./Calendario";
import ContentLoader from "react-content-loader";
import ImagenDefault from "../../../assets/img/baking.png";
import Fuse from "fuse.js";
import Alert from "react-s-alert";
import LoadingSpinner from "../../../assets/img/oval.svg";
import ReactPaginate from "react-paginate";

class VerEventos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedPage: "",
      calendarActive: true,
      currentLatestEvents: [],
      modal: false,
      eventTitle: "",
      eventDescription: "",
      eventDate: "",
      eventPicture: "",
      eventSlug: "",
      slug: "",
      search: "",
      isLoading: true,
      deleteEvents: {
        deleting: false
      },
      dropdownOpen: false,
      showConfirmation: false
    };

    this.toggle = this.toggle.bind(this);
    this.getCalendar = this.getCalendar.bind(this);
    this.getList = this.getList.bind(this);
  }

  toggleConfirmation = ({slug = ""}={})=> this.setState(state =>({
    showConfirmation: !state.showConfirmation,
    eventSlug: slug
  }));

  updateSearch(event) {
    this.setState({
      search: event.target.value.substr(0, 40)
    });
  }

  componentDidMount() {
    this.fetchLatestEvents(0);
  }

  fetchLatestEvents = async range => {
    this.setState({
      isLoading: true
    });
    const {error, data} = await api.getLatestEvents(range);
    if (error) {
      console.log(error);
    }
    if (data) {
      this.setState({
        currentLatestEvents: data
        // totalEvents: data.totalEvents
      });
    }
    this.setState({
      isLoading: false
    });
  };

  getLatestEvents() {
    const es_MX = {
      months: [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
      ]
    };
    const strftimeES = strftime.localize(es_MX);
    const {currentLatestEvents = []} = this.state;
    let options = {
      shouldSort: true,
      threshold: 0.2,
      tokenize: true,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: ["titulo", "descripcion"]
    };
    const fuse = new Fuse(currentLatestEvents, options);
    const result = fuse.search(this.state.search || "");
    const eventsToShow = this.state.search ? result : currentLatestEvents;
    // if (eventsToShow.length !== 0) {
    // }
    return eventsToShow.map(current => (
      <Col xs="12" md="6" lg="4" key={current.id}>
        <Card className="event-card">
          <CardImg
            top
            width="318px"
            height="200px"
            src={current.portada == null ? ImagenDefault : current.portada}
            alt={current.titulo}
          />
          <CardBody className="calendario-card">
            <CardTitle className="event-title">{current.titulo}</CardTitle>
            <CardSubtitle className="event-date">
              {strftimeES("%B %d, %Y", new Date(current.fechaInicio))}
            </CardSubtitle>
            <CardText className="event-description">
              <p dangerouslySetInnerHTML={{ __html: current.descripcion }}/>
            </CardText>
          </CardBody>
          <CardFooter>
            <div className="btn-wrapper">
              <Button
                className="btn btn-pill btn-light"
                onClick={() =>
                    this.toggle(
                      current.titulo,
                      current.descripcion,
                      current.fechaInicio,
                      current.fechaFin,
                      current.portada,
                      current.slug
                    )
                  }
              >
                <i className="icon-eye"/> Ver
              </Button>
              <Button
                className="btn btn-pill btn-dark"
                onClick={() =>
                    this.props.history.push(
                      `/panel/modificar-eventos/${current.slug}`
                    )
                  }
              >
                <i className="icon-note"/> Modificar
              </Button>
              {/*
               <Button
               className="btn btn-pill btn-danger"
               onClick={() => this.deleteModal(current.slug)}
               >
               {this.state.deleteEvents.deleting && this.state.deleteEvents.slug === current.slug ? (
               <img
               src={LoadingSpinner}
               className="spinner"
               alt="Cargando..."
               width="20px"
               />
               ) : (
               <Fragment>
               <i className="icon-trash" /> Eliminar
               </Fragment>
               )}
               </Button>
               */}
              <Button
                className="btn btn-pill btn-danger"
                onClick={
                    () => this.toggleConfirmation(current)
                  }>
                <Fragment>
                  <i className="icon-trash"/> Eliminar
                </Fragment>
              </Button>
            </div>
          </CardFooter>
        </Card>
      </Col>
    ));
  }

  toggle(titulo, descripcion, fechaInicio, fechaFin, portada, slug) {
    this.setState({
      modal: !this.state.modal,
      eventTitle: titulo,
      eventDescription: descripcion,
      eventStart: fechaInicio,
      eventEnd: fechaFin,
      eventPicture: portada,
      eventSlug: slug
    });
  }


  deleteModal = async slug => {
    this.setState({
      deleteEvents: {
        deleting: true,
        slug
      }
    });

    const {data} = await api.deleteEvents({
      slug: slug
    });

    if (data) {
      if (data.status === 200) {
        this.setState({
          deleteEvents: {
            deleting: false
          },
          showConfirmation: false
        });
        setTimeout(() => this.fetchLatestEvents(this.state.selectedPage), 1000);
        return Alert.success("Evento eliminado", {
          position: "bottom-right",
          effect: "slide",
          timeout: 5000,
          offset: 30
        });
      } else {
        this.setState({
          deleteEvents: {
            deleting: false
          }
        });
        return Alert.error("Hubo un error al eliminar el evento", {
          position: "bottom-right",
          effect: "slide",
          timeout: 5000,
          offset: 30
        });
      }
    }
  };

  getCalendar() {
    this.setState({
      calendarActive: false
    });
  }

  getList() {
    this.setState({
      calendarActive: true
    });
  }

  handlePageClick(data) {
    const selected = data.selected;
    this.setState({
      selectedPage: selected
    });
    this.fetchLatestEvents(selected);
  }

  alertStack = {limit: 1};

  render() {
    const pages = this.state.totalEvents / 12;
    const latestEvents = this.getLatestEvents();
    const GetEventsContentLoader = props => (
      <ContentLoader
        rtl
        height={700}
        width={1920}
        speed={2}
        primaryColor="#C0BDBD"
        secondaryColor="#989191"
        {...props}
      >
        <rect x="673.29" y="152.67" rx="0" ry="0" width="0" height="0"/>
        <rect x="126.29" y="39.33" rx="0" ry="0" width="484.92" height="47"/>
        <rect x="1672.96" y="39.33" rx="0" ry="0" width="160.93" height="47"/>
        <rect
          x="128.88"
          y="118.33"
          rx="0"
          ry="0"
          width="501.9"
          height="544.5"
        />
        <rect
          x="1327.88"
          y="119.33"
          rx="0"
          ry="0"
          width="501.9"
          height="544.5"
        />
        <rect
          x="730.88"
          y="121.33"
          rx="0"
          ry="0"
          width="501.9"
          height="544.5"
        />
      </ContentLoader>
    );
    const es_MX = {
      months: [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
      ]
    };
    const strftimeES = strftime.localize(es_MX);
    return (
      <Fragment>
        <Alert stack={this.alertStack}/>
        <Modal
          isOpen={this.state.modal}
          toggle={() => this.toggle()}
          className={this.props.className}
        >
          <img
            src={this.state.eventPicture}
            width="100%"
            alt={this.state.eventTitle}
          />
          <ModalBody>
            <Row className="text-center bg-modal-header p-2">
              <Col xs="12">
                <h2 className="event-description-title">
                  {this.state.eventTitle}
                </h2>
              </Col>
              <Col xs="6">
                <span className="font-lg">
                  <i className="icon-calendar"/> Fecha Inicio
                </span>
                <br />
                <Badge
                  className="mr-1 p-3 mt-2 mb-4 event-date-modal"
                  color="secondary"
                >
                  {strftimeES("%B %d, %Y", new Date(this.state.eventStart))}
                </Badge>
              </Col>
              <Col xs="6">
                <span className="font-lg">
                  <i className="icon-calendar"/> Fecha Fin
                </span>
                <br />
                <Badge
                  className="mr-1 p-3 mt-2 mb-4 event-date-modal"
                  color="secondary"
                >
                  {strftimeES("%B %d, %Y", new Date(this.state.eventEnd))}
                </Badge>
              </Col>
            </Row>
            <p
              className="event-description-text"
              dangerouslySetInnerHTML={{ __html: this.state.eventDescription }}
            />
          </ModalBody>
          <ModalFooter>
            <div className="modal-wrapp">
              <div className="modal-view-tn">
                <Button
                  className="btn-pill btn-lg"
                  onClick={() =>
                    window.open(
                      `https://www.tenemosnoticias.com/eventos/${
                        this.state.eventSlug
                      }`
                    )
                  }
                >
                  <i className="icon-eye"/> Ver en TN
                </Button>
              </div>
              <div className="modal-options">
                <Button
                  className="btn-pill btn-lg btn-light"
                  onClick={() =>
                    this.props.history.push(
                      `/panel/modificar-eventos/${this.state.eventSlug}`
                    )
                  }
                >
                  <i className="icon-note"/> Modificar
                </Button>
                <Button
                  className="btn-pill btn-lg btn-dark"
                  onClick={() => this.toggle()}
                >
                  <i className="icon-logout"/> Cancelar
                </Button>
              </div>
            </div>
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.showConfirmation}
          toggle={this.toggleConfirmation}
          className={this.props.className}
        >
          <ModalBody>
            <h2 className="mensConfirmar">
              ¿Está seguro que desea eliminar este evento?
            </h2>
          </ModalBody>
          <ModalFooter>
            <div className="modal-wrapp">

              <div className="modal-options">
                <Button
                  className="btn-pill btn-lg btn-success"
                  onClick={() => this.deleteModal(this.state.eventSlug)}
                >
                  {this.state.deleteEvents.deleting && this.state.deleteEvents.slug === this.state.eventSlug ? (
                    <img
                      src={LoadingSpinner}
                      className="spinner"
                      alt="Cargando..."
                      width="20px"
                    />
                  ) :
                  <Fragment>
                    <i className="icon-check"/> Aceptar
                  </Fragment>
                  }
                </Button>
                <Button
                  className="btn-pill btn-lg btn-dark"
                  onClick={() => this.toggleConfirmation()}
                >
                  <i className="icon-logout"/> Cancelar
                </Button>
              </div>
            </div>
          </ModalFooter>
        </Modal>
        {this.state.currentLatestEvents ? (
          this.state.calendarActive ? (
            <Row className="p-3 mb-5 calendario">
              <Col xs="12" md="4" className="text-left mb-3">
                <input
                  className="form-control search-events"
                  placeholder="Buscar evento"
                  value={this.state.search}
                  onChange={this.updateSearch.bind(this)}
                />
              </Col>
              <Col
                xs="12"
                md="8"
                className="show-calendario-btn text-right mb-3"
              >
                <Button
                  className="btn-material-secondary"
                  onClick={() => this.getCalendar()}
                >
                  <i className="icon-calendar"/> Ver calendario
                </Button>
              </Col>
              {latestEvents}
              <Col xs="12" className="pagination-wrapper">
                <ReactPaginate
                  pageCount={pages}
                  pageRangeDisplayed={3}
                  previousLabel={`«`}
                  nextLabel={"»"}
                  breakLabel={"..."}
                  initialPage={this.state.currentPage}
                  onPageChange={this.handlePageClick.bind(this)}
                  disableInitialCallback={true}
                  containerClassName={"pagination"}
                  pageClassName={"pagination-page"}
                />
              </Col>
            </Row>
          ) : (
            <Row className="p-3 mb-5 calendario ">
              <Col xs="12" className="text-right mb-3">
                <Button
                  className="btn-material-secondary"
                  onClick={() => this.getList()}
                >
                  <i className="icon-list"/> Ver lista
                </Button>
              </Col>
              <Calendario
                events={this.state.currentLatestEvents}
                toggle={this.toggle}
                history={this.props.history}
              />
            </Row>
          )
        ) : this.state.isLoading ? (
          <div className="w-100">
            <GetEventsContentLoader />
          </div>
        ) : (
          <Row className="p-3 mb-5 calendario">
            <div className="nof-container">
              <div className="noe"/>
            </div>
          </Row>
        )}
      </Fragment>
    );
  }
}

export default VerEventos;
