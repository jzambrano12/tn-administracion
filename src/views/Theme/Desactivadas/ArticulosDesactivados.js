import React, { Component } from "react";
import strftime from "strftime";
import Alert from "react-s-alert";
import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/slide.css";
import api from "../../../api";
import {
  Row,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Col,
  CardDeck,
  Card,
  // CardImg,
  CardBody,
  // CardSubtitle,
  CardText,
  CardTitle
  // Badge
} from "reactstrap";

class ArticulosDesactivados extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false,
      currentInactiveArticle: [],
      currentImage: null,
      selectedArticle: [],
      clicActivate: false,
      tituloArticulo: "",
      idArticulo: ""
    };
    this.toggleSuccess = this.toggleSuccess.bind(this);
  }

  componentDidMount() {
    this.fetchInactiveArticle();
  }

  shouldComponentUpdate(nextState) {
    if (
      this.state.currentInactiveArticle !== nextState.currentInactiveArticle
    ) {
      return true;
    }
    return false;
  }

  fetchInactiveArticle = async () => {
    const { error, data } = await api.getInactiveArticle();

    if (error) {
      this.setState({
        clicActivate: false
      });
    }

    if (data) {
      this.setState({
        currentInactiveArticle: data,
        clicActivate: false
      });
      Alert.success(
        this.state.currentInactiveArticle.length === 1
          ? this.state.currentInactiveArticle.length + " Articulo Desactivado"
          : this.state.currentInactiveArticle.length +
              " Articulos Desactivados",
        {
          position: "bottom-right",
          effect: "slide",
          timeout: 5000,
          offset: 30
        }
      );
    }
  };

  fetchActivar = async id => {
    const { error } = await api.isActiveArticle(id);
    if (error) {
      this.setState({
        success: !this.state.success
      });
      Alert.error("Esta noticia ya esta activada ", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
    } else {
      this.setState({
        success: !this.state.success
      });
      Alert.success("Noticia Activada", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
      setTimeout(() => this.fetchInactiveArticle(), 1000);
    }
  };

  toggleSuccess(titulo, id) {
    this.setState({
      success: !this.state.success,
      tituloArticulo: titulo,
      idArticulo: id
    });
  }

  getInactiveArticles() {
    const es_MX = {
      months: [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
      ]
    };
    const strftimeES = strftime.localize(es_MX);
    const { currentInactiveArticle } = this.state;
    return currentInactiveArticle.map(current => (
      <Col xs="12" md="12" className="mt-3 mb-3" key={current.id}>
        <CardDeck>
          <Card className="card-remove">
            <CardBody>
              <Row>
                <Col md="12" className="status-article">
                  <CardText>
                    {strftimeES(
                      "%B %d, %Y",
                      new Date(current.fechaPublicacion)
                    )}
                  </CardText>
                </Col>
              </Row>
              <Row>
                <Col md="12" className="title-article mt-4">
                  <CardTitle>{current.titulo}</CardTitle>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col md="12" className="text-right">
                  <div className="button-align">
                    <Button
                      className="btn-pill mr-3"
                      color="success"
                      onClick={() =>
                        this.toggleSuccess(current.titulo, current.id)
                      }
                    >
                      <i className="icon-check" /> Activar Articulo
                    </Button>
                  </div>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </CardDeck>
      </Col>
    ));
  }

  alertStack = { limit: 1 };

  render() {
    const inactives = this.getInactiveArticles();
    if (inactives.length === 0) {
      return (
        <Row className="p-3 mb-5 calendario">
          <div className="nof-container">
            <div className="noa" />
          </div>
        </Row>
      );
    } else {
      return (
        <div className="animated fadeIn h-100">
          <Alert stack={this.alertStack} />
          <Row className="p-3 mb-5 calendario">{inactives}</Row>

          <Modal
            isOpen={this.state.success}
            toggle={() => this.toggleSuccess()}
            className={"modal-success " + this.props.className}
          >
            <ModalHeader toggle={() => this.toggleSuccess()}>
              ¿Realmente desea activar esta noticia?
            </ModalHeader>
            <ModalBody className="text-center">
              <div className="w-75 m-auto">
                <h3 className="mt-2">{this.state.tituloArticulo}</h3>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="secondary"
                onClick={() => this.fetchActivar(this.state.idArticulo)}
              >
                Activar
              </Button>
              <Button color="success" onClick={() => this.toggleSuccess()}>
                Cancelar
              </Button>
            </ModalFooter>
          </Modal>
        </div>
      );
    }
  }
}

export default ArticulosDesactivados;
