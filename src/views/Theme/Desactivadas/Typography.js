import React, { Component } from "react";
import {
  Row,
  Col,
  Badge,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Card,
  CardImg,
  CardTitle,
  CardText,
  CardDeck,
  CardSubtitle,
  CardBody
} from "reactstrap";
import strftime from "strftime";
import Alert from "react-s-alert";
import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/slide.css";
import api from "../../../api";
import ImagenDefault from "../../../assets/img/baking.png";

class Typography extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false,
      currentInactiveArticle: [],
      currentImage: null,
      selectedArticle: [],
      clicActivate: false,
      tituloArticulo: "",
      slugArticulo: "",
      imagenArticulo: ""
    };
    this.toggleSuccess = this.toggleSuccess.bind(this);
  }

  toggleSuccess(titulo, slug, imagen) {
    this.setState({
      success: !this.state.success,
      tituloArticulo: titulo,
      slugArticulo: slug,
      imagenArticulo: imagen
    });
  }

  componentDidMount() {
    this.fetchInactiveNotice();
  }

  shouldComponentUpdate(nextState) {
    if (
      this.state.currentInactiveArticle !== nextState.currentInactiveArticle
    ) {
      return true;
    }

    return false;
  }

  fetchInactiveNotice = async () => {
    const { error, data } = await api.getInactiveNotice();

    if (error) {
      this.setState({
        clicActivate: false
      });
    }

    if (data) {
      this.setState({
        currentInactiveArticle: data,
        clicActivate: false
      });
      Alert.success(
        this.state.currentInactiveArticle.length === 1
          ? this.state.currentInactiveArticle.length + " Noticia Desactivada"
          : this.state.currentInactiveArticle.length + " Noticias Desactivadas",
        {
          position: "bottom-right",
          effect: "slide",
          timeout: 5000,
          offset: 30
        }
      );
    }
  };

  getInactiveNotice() {
    const es_MX = {
      months: [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
      ]
    };
    const strftimeES = strftime.localize(es_MX);
    const { currentInactiveArticle } = this.state;
    return currentInactiveArticle.map(current => (
      <Col xs="12" md="12" className="mt-3 mb-3" key={current.id}>
        <CardDeck>
          <Card className="card-remove">
            {current.images[0].dimensionesDisponibles[0] ? (
              <CardImg
                top
                className="card-img-dimensions"
                src={current.images[0].dimensionesDisponibles[0].url}
                alt={current.titulo}
              />
            ) : (
              <CardImg
                top
                className="card-img-dimensions"
                src={ImagenDefault}
                alt={current.titulo}
              />
            )}
            <CardBody>
              <Row>
                <Col md="12" className="status-article">
                  <CardSubtitle>{current.categoria.nombre}</CardSubtitle>
                  <CardText>
                    {strftimeES(
                      "%B %d, %Y",
                      new Date(current.fechaPublicacion)
                    )}
                  </CardText>
                </Col>
              </Row>
              <Row>
                <Col md="12" className="title-article mt-4">
                  <CardTitle>{current.titulo}</CardTitle>
                </Col>
              </Row>
              <Row>
                <Col md="12" className="interaction-article">
                  <CardText>
                    <Badge color="light" className="p-2 mr-3">
                      <i className="icon-eye font-2xl" />{" "}
                      <span className="views-card">{current.vistas}</span>
                    </Badge>
                    <Badge color="secondary" className="p-2">
                      <i className="icon-share-alt font-2xl" />{" "}
                      <span className="views-card">{current.compartidos}</span>
                    </Badge>
                  </CardText>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col md="12" className="text-right">
                  {/*{console.log(current)}*/}
                  <div className="button-align">
                    <Button
                      className="btn-pill mr-3"
                      color="success"
                      onClick={() =>
                        this.toggleSuccess(
                          current.titulo,
                          current.slug,
                          current.images[0].dimensionesDisponibles[1].url
                        )
                      }
                    >
                      <i className="icon-check" /> Activar Noticia
                    </Button>
                  </div>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </CardDeck>
      </Col>
    ));
  }

  fetchActivar = async slug => {
    const { error } = await api.isActiveNotice(slug);
    if (error) {
      this.setState({
        success: !this.state.success
      });
      Alert.error("Esta noticia ya esta activada ", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
    } else {
      this.setState({
        success: !this.state.success
      });
      Alert.success("Noticia Activada", {
        position: "bottom-right",
        effect: "slide",
        timeout: 5000,
        offset: 30
      });
      setTimeout(() => this.fetchInactiveNotice(), 1000);
    }
  };

  alertStack = { limit: 1 };

  render() {
    const inactives = this.getInactiveNotice();
    if (inactives.length === 0) {
      return (
        <Row className="p-3 mb-5 calendario">
          <div className="nof-container">
            <div className="nof" />
          </div>
        </Row>
      );
    } else {
      return (
        <div className="animated fadeIn h-100">
          <Alert stack={this.alertStack} />
          <Row className="p-3 mb-5 calendario">{inactives}</Row>

          <Modal
            isOpen={this.state.success}
            toggle={() => this.toggleSuccess()}
            className={"modal-success " + this.props.className}
          >
            <ModalHeader toggle={() => this.toggleSuccess()}>
              ¿Realmente desea desactivar esta noticia?
            </ModalHeader>
            <ModalBody className="text-center">
              <div className="w-75 m-auto">
                <img
                  src={this.state.imagenArticulo}
                  alt={this.state.tituloArticulo}
                  width="85%"
                />
                <h3 className="mt-2">{this.state.tituloArticulo}</h3>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="secondary"
                onClick={() => this.fetchActivar(this.state.slugArticulo)}
              >
                Activar
              </Button>
              <Button color="success" onClick={() => this.toggleSuccess()}>
                Cancelar
              </Button>
            </ModalFooter>
          </Modal>
        </div>
      );
    }
  }
}

export default Typography;
