import axios from "axios";
import store from "./configureStore";

const api = (() => {
  const state = store.getState();
  const api = axios.create({
    // baseURL: "http://localhost:3000",
    // baseURL: "http://192.168.0.7:3000",
    baseURL: "https://admin.tenemosnoticias.com/api",
    headers:
      state.user && state.user.token ? { Authorization: state.user.token } : {}
  });

  store.subscribe(() => {
    const state = store.getState();
    const headers = api.defaults.headers.common;
    if (headers.Authorization !== state.user.token) {
      if (state.user && state.user.token) {
        headers.Authorization = state.user.token;
      } else if (headers.Authorization) {
        delete headers.Authorization;
      }
    }
  });

  return api;
})();

const handleResponse = ({ data, status }) => ({ data, status });

const handleError = error => {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    const {
      data: { message },
      status
    } = error.response;
    return {
      message: status >= 500 ? `Error interno (${status})` : message,
      error
    };
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    return { message: "Error interno", error };
  }
  return { message: "Error de configuracion", error };
};

const handleRequest = request => request.then(handleResponse, handleError);

export default {
  getLatestEvents: range =>
    handleRequest(api.get(`/event/latest-events?page=${range}`)),
  deleteEvents: data => handleRequest(api.post(`/event/delete-event`, data)),
  changeEvents: slug => handleRequest(api.get(`/event/find-by-slug/${slug}`)),
  updateEvent: data => handleRequest(api.post(`/event/update-event`, data)),
  createEvent: data => handleRequest(api.post(`/event/create-event`, data)),
  getSearch: linkSearch =>
    handleRequest(api.get(`/story/search-news/${linkSearch}`)),
  getInactiveNotice: () => handleRequest(api.get(`/story/inactive-stories`)),
  getInactiveArticle: () => handleRequest(api.get(`/news/inactive-news`)),
  getArticle: (slug, id) =>
    handleRequest(api.get(`/story/find-by-new/${slug}/${id}`)),
  getCategories: () => handleRequest(api.get(`/category/default`)),
  login: data => handleRequest(api.post("/auth/authenticate", data)),
  getUserInfo: token =>
    handleRequest(
      api.post("/auth/auto-login", null, { headers: { Authorization: token } })
    ),
  isNotActiveNotice: slug =>
    handleRequest(api.post(`/story/change-status/${slug}/false`)),
  isActiveNotice: slug =>
    handleRequest(api.post(`/story/change-status/${slug}/true`)),
  // isNotActiveArticle: (slug, id) => handleRequest(api.post(`/story/change-status/${slug}/${id}/false`)),
  isActiveArticle: id => handleRequest(api.post(`/news/restore-news/${id}`)),
  updateArticle: data => handleRequest(api.post(`/story/update-story`, data))
};
