import Colors from "./views/Theme/Inicio/Colors";
import Typography from "./views/Theme/Desactivadas/Typography";
import ArticulosDesactivados from "./views/Theme/Desactivadas/ArticulosDesactivados";
import VerEventos from "./views/Theme/Eventos/VerEventos";
import CrearEventos from "./views/Theme/Eventos/CrearEventos";
import ModificarEventos from "./views/Theme/Eventos/ModificarEventos";
// import Page404 from './views/Pages/Page404/Page404';

const routes = [
  { path: "/panel/resultado/:search?", name: "", component: Colors },
  { path: "/panel/editar/:slug?/:id?", name: "", component: Colors },
  { path: "/panel/noticias-desactivadas", name: "", component: Typography },
  {
    path: "/panel/articulos-desactivados",
    name: "",
    component: ArticulosDesactivados
  },
  { path: "/panel/ver-eventos", name: "", component: VerEventos },
  { path: "/panel/crear-eventos", name: "", component: CrearEventos },
  {
    path: "/panel/modificar-eventos/:newSlug",
    name: "",
    component: ModificarEventos
  }
];

export default routes;
